# Є два списки, які наповнюються користувачем з клавіатури. Сформувати
# список, в якому будуть міститися унікальні значення першого відносно
# другого списку та навпаки без повторень. Роздрукувати підсумковий об'єкт
# на екран в прямій послідовності, зворотній, а також виконати сортування за
# зростанням та спаданням.

l1 = [-1, 1, 2, 3, -4, 4, 5, -5]
l2 = [4, -5, 6, 7, 8]

res_list = []

for elem in l1:
    if elem not in l2:
        res_list.append(elem)

for elem in l2:
    if elem not in l1:
        res_list.append(elem)

print(res_list)
res_list.reverse()
print(res_list)
res_list.sort()
print(res_list)
res_list.sort(reverse=True)
print(res_list)

res_list.sort(key=str)
print(res_list)
